package comparingInJava;

public class Main {
    public static void main(String[] args) {
        Employee employee = new Employee("Wojciech", 2000);
        employee.raiseSalary(50);
        System.out.println(employee);

        Employee employee2 = new Employee("Jan", 3000.50);
        System.out.println(employee.compareTo(employee2) > 0 ? "Wieksze" : "mniejsze");
    }
}
