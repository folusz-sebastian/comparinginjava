package comparingInJava;

public class Employee implements Comparable<Employee>{
    private final String name;
    private double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    public void raiseSalary (double byPercent){
        this.salary *= (1 + byPercent/100);
    }

    @Override
    public String toString() {
        return name + ", " + salary;
    }

    @Override
    public int compareTo(Employee o) {
        return Double.compare(salary, o.salary);
    }
}
